use winit::event::*;

pub struct Camera {
    pub position: cgmath::Vector3<f32>,
    pub amount_left: f32,
    pub amount_right: f32,
    pub amount_up: f32,
    pub amount_down: f32,
}

impl Camera {
    pub fn new(position: impl Into<cgmath::Vector3<f32>>) -> Self {
        Self {
            position: position.into(),
            amount_left: 0.0,
            amount_right: 0.0,
            amount_up: 0.0,
            amount_down: 0.0
        }
    }
    pub fn to_normal(&self) -> [f32; 3] {
        [self.position.x, self.position.y, self.position.z] 
    }
    pub fn process_keyboard(&mut self, key: VirtualKeyCode, state: ElementState) -> bool {
        let amount = if state == ElementState::Pressed {1.0} else {0.0};
        match key {
            VirtualKeyCode::W => {
                self.amount_up = amount;
                true
            },
            VirtualKeyCode::S => {
                self.amount_down = amount;
                true
            },
            VirtualKeyCode::A => {
                self.amount_left = amount;
                true
            },
            VirtualKeyCode::D => {
                self.amount_right = amount;
                true
            },
            _ => false
        }
    }
}

