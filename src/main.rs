use concrete::run;

fn main() {
    pollster::block_on(run());
}
