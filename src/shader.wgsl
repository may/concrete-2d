// Vertex shader

@group(1) @binding(0) // 1.
var<uniform> camera_position: vec3<f32>;
@group(2) @binding(0)
var<uniform> screen_ratio: vec2<f32>;

struct VertexInput {
    @location(0) position: vec3<f32>,
    @location(1) tex_coords: vec2<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
}

struct InstanceInput {
    @location(5) position: vec3<f32>,
    @location(6) rotation_1: vec3<f32>,
    @location(7) rotation_2: vec3<f32>,
    @location(8) rotation_3: vec3<f32>,
};

@vertex
fn vs_main(
    model: VertexInput,
    instance: InstanceInput,
) -> VertexOutput {

    let rotation_matrix = mat3x3<f32>(
        instance.rotation_1,
        instance.rotation_2,
        instance.rotation_3,
    );

    var out: VertexOutput;
    out.tex_coords = model.tex_coords;

    //let rotated_position = rotation_matrix * model.position;
    let rotated_position = model.position;
    let screen_position = (rotated_position - camera_position + instance.position);
    let scaled_position = vec3<f32>(screen_position[0] / screen_ratio[0], screen_position[1], screen_position[2]);
    out.clip_position = vec4<f32>(scaled_position * screen_ratio[1], 1.0);

    return out;
}

// Fragment shader

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0)@binding(1)
var s_diffuse: sampler;

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return textureSample(t_diffuse, s_diffuse, in.tex_coords);
}
